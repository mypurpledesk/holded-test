# SUMMARY #

This is a small application to store users details, it can be seen at https://shrouded-caverns-39250.herokuapp.com/,

### How to run the code ###

  * 1 - Copy this repository in your local machine or clone it using -> git clone git@bitbucket.org:mypurpledesk/holded-test.git
  * 2 - open a terminal inside "holded-test" folder and run -> "npm install" and then "npm run dev"
  * 3 - open a terminal inside "holded-test/client" folder and run -> "npm" install and then "npm run dev"
  * 4 - Open your browser at "localhost:8080" to see the client in action
  * 5 - To build the Vuejs client open a terminal inside "holded-test/client" folder and run -> "npm run build"

Optional you can change the mongoDB connection string in the second line of "holded-test/server/routes/api/users.js" (I will disable this db as soon you checked out this test),

### What is working in dev environment ###

* Add contact
* Remove contact
* Modify contact photo and details
* Pagination

### What is not ###

* Pagination disabled state.

### ISSUES ###

* Images storage work only in dev env due a problem on my AWS account, Heroku deployment ->  https://shrouded-caverns-39250.herokuapp.com/ can't store images.

### What can be done ###

 * Better animation
 * Responsiveness
 * Images resizing and crop
 * Localization
