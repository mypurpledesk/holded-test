import axios from 'axios';

/* eslint-disable */
///URL API EXRPRESS JS
const url = "api/users/"


class UserRequest {
  //GET ALL USERS
  static getUsers() {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.get(url)
        const data = res.data
        resolve(
          data.map(user => ({
            ...user,
            createdAt: new Date(user.createdAt)
          }))
        )
      } catch (err) {
        reject(err)
      }
    })
  }

  //ADD USER
  static insertUser(userParams) {
    const formData = new FormData();
    var name = 'pic'
    formData.append(name, userParams.pic);
    formData.append('params', JSON.stringify(userParams));
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    };
    return axios.post(url, formData, config)
  }

  static deleteUser(id) {
    return axios.delete(url + id)
  }

  //GET SINGLE USER
  static getUser(id) {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.get(url + id)
        const data = res.data
        resolve(
          data.map(user => ({
            ...user,
            id: id
          }))
        )
      } catch (err) {
        reject(err)
      }
    })
  }

  //UPDATE USER
  static updateUser(userParams) {
    const formData = new FormData();
    var name = 'pic'
    formData.append(name, userParams.pic);
    formData.append('params', JSON.stringify(userParams));
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    };
    return axios.post(url + 'update/' + userParams._id, formData, config)
  }

  //FILL CANVAS ELEMENT WITH SELECTED IMAGE
  static getDummyImage(file) {
    return new Promise((resolve, reject) => {
      var reader = new FileReader();
      reader.onload = function(event) {
        var img = new Image();
        img.onload = function() {
          var canvas = document.getElementById("dummyPhoto");
          var ctx = canvas.getContext("2d");
          ctx.clearRect(0, 0, canvas.width, canvas.height);
          ctx.drawImage(img, 0, 0, 300, 300 * img.height / img.width)
        }
        console.log(img.src);
        img.src = event.target.result;
      }
      reader.readAsDataURL(file);
    })
  }

  //CLEAR CANVAS AFTER UPLOAD
  static clearCanvas() {
    var canvas = document.getElementById("dummyPhoto");
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }
}
export default UserRequest
