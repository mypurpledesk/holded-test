///CHANGE THIS LINE TO USE YOUR MONGODB
const mongoDBConnectionString = 'mongodb+srv://dbuser:dYUOdBcL8j756zwy@holdedtest-f4vwa.mongodb.net/test?retryWrites=true'

const express = require('express')
const mongodb = require('mongodb')
const multer = require('multer')


const router = express.Router()
const uploadDirectory = './client/public/images/uploads/'
var Storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, "./uploads");
  },
  filename: function(req, file, callback) {
    callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
  }
});

var upload = multer({
  dest: uploadDirectory
});


//GET posts
router.get('/', async (req, res) => {
  const users = await loadUsers()
  res.send(await users.find({}).toArray())
})
module.exports = router

//ADD posts
router.post('/', upload.single('pic'), async (req, res) => {
  var params = JSON.parse(req.body.params)

  var filename = ''
  if (req.file) {
    filename = req.file.filename
  }

  const users = await loadUsers()
  await users.insertOne({
    fname: params.fname,
    lname: params.lname,
    email: params.email,
    position: params.position,
    office: params.office,
    salary: params.salary,
    weekly: params.weekly,
    company: params.company,
    pic: filename,
    createdAt: new Date()
  })
  res.status(201).send()
})
//DELETE post
router.delete('/:id', async (req, res) => {
  const users = await loadUsers()
  await users.deleteOne({
    _id: new mongodb.ObjectID(req.params.id)
  })
  res.status(200).send()
})

//GET USER DETAILS
router.get('/:id', async (req, res) => {
  const users = await loadUsers()
  res.send(
    await users.find({
      _id: new mongodb.ObjectID(req.params.id)
    }).toArray()
  )

})
//UPDATE USER DETAILS
router.post('/update/:id', upload.single('pic'), async (req, res) => {
  var params = JSON.parse(req.body.params)
  const users = await loadUsers()
  if (req.file) {
    await users.updateOne({
      '_id': new mongodb.ObjectID(req.params.id)
    }, {
      $set: {
        pic: req.file.filename,
      }

    })
  }
  await users.updateOne({
    '_id': new mongodb.ObjectID(req.params.id)
  }, {
    $set: {
      fname: params.fname,
      lname: params.lname,
      email: params.email,
      position: params.position,
      office: params.office,
      salary: params.salary,
      weekly: params.weekly,
      company: params.company,
      modificatedAt: new Date()
    }
  })
  res.status(201).send('req.params.id')
})

//LOAD ALL USERS
async function loadUsers() {
  const client = await mongodb.MongoClient.connect(mongoDBConnectionString, {
    useNewUrlParser: true
  })
  return client.db('holdedtest').collection('users')
}
