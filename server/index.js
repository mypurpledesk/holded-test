const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express();

///middleware

app.use(bodyParser.json())
app.use(cors())

const users = require('./routes/api/users')

app.use('/api/users/', users)


///PRODUCTION
if (process.env.NODE_ENV === 'production') {
  ///STAIC FOLDER
  app.use(express.static(__dirname + '/public/'))
  /// SPA SEND ALL OTHER ROUTES TO INDEX
  app.get(/.*/, (req, res) => res.sendFile(__dirname + '/public/index.html'))
  }

  const port = process.env.PORT || 5000;
  app.listen(port, () => console.log('server port: ' + port))
